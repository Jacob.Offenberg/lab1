package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        // TODO: Implement Rock Paper Scissors

        System.out.println("Let's play round " + roundCounter++);

		outerloop:

        while(true) {
			System.out.println("Your choice (Rock/Paper/Scissors)?");
			String myMove = sc.nextLine();
			
			//Check for valid myMove input
			
			if(!myMove.equals("rock") && !myMove.equals("paper") && !myMove.equals("scissors")) {
				System.out.println("I don't understand " + myMove + ". Try again.");
			} else {
				//Randomly generate opponents move
				
				int rand = (int)(Math.random() * 3);
				String opponentsMove = "";
				if (rand == 0) {
					opponentsMove = "rock";
				} else if (rand == 1) {
					opponentsMove = "paper";
				} else {
					opponentsMove = "scissors";
				}
				System.out.print("Human chose "+ myMove +", computer chose " + opponentsMove);
				
				//Calculate if  the user won lost or tied
				
				if(myMove.equals(opponentsMove)) {
					System.out.println("Its a  tie!");
				} else if((myMove.equals("rock") && 
				opponentsMove.equals("scissors")) || (myMove.equals
				("paper") && opponentsMove.equals("rock")) || 
				(myMove.equals("scissors") && opponentsMove.equals("paper"))){
					System.out.println("You won!");
					humanScore++;
				} else {
					System.out.println("You lost!");
					computerScore++;
				}
                System.out.println("Score: human "+ humanScore + ", computer " + computerScore);

			while(true) {
                System.out.println("Do you wish to continue playing? (y/n)?");
				String newRound = sc.nextLine();
				if (newRound.equals("y")) {
                    System.out.println("Let's play round " + roundCounter++);
                    break;
				} else if(!newRound.equals("y") && !newRound.equals("n")) {
                    System.out.println("I dont understand " + newRound + ". Try again.");   
                } else {
                    System.out.println("Bye bye :)");
                    break outerloop;
                }			
            }
		
			}
		    
			}
        }
		
    

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
